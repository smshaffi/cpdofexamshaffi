package org.agiletestingalliance.cpdof.reexam;

import org.agiletestingalliance.cpdof.reexam.model.CertificationType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ATA Support team Feb 2020
 */
public class ATAService {

    public List getAvailableBrands(CertificationType type){

        List brands = new ArrayList();

        if(type.equals(CertificationType.CPSAT)){
            brands.add("https://cpsat.agiletestingalliance.org/");
            brands.add("https://cpsat.agiletestingalliance.org/#eventswe");
            brands.add("https://cpsat.agiletestingalliance.org/blogs/");
            brands.add("https://agiletestingalliance.org/certified-alumni/#cp-sat");
            

        }else if(type.equals(CertificationType.CPDOF)){
            brands.add("https://cpdof.devopsppalliance.org/");
            brands.add("https://cpdof.devopsppalliance.org/#events");
            brands.add("https://cpsat.agiletestingalliance.org/blogs/");

        }else if(type.equals(CertificationType.CPWST)){
            brands.add("https://cpdof.devopsppalliance.org/");
            brands.add("https://cpdof.devopsppalliance.org/#events");
            brands.add("https://cpsat.agiletestingalliance.org/blogs/");

        }else {
            brands.add("https://agiletestingalliance.org/agile-testing/");
            brands.add("https://agiletestingalliance.org/devopsppalliance/");
            
        }
    return brands;
    }
}
